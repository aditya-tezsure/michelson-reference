import os
from language_def import LanguageDefinition
import subprocess




def main():
    ldef = LanguageDefinition(False)
    for instr in ldef.get_instructions():
        if not 'examples' in instr:
            # print("No examples for " + instr)
            continue

        for ex in instr['examples']:
            if not 'path' in ex:
                continue
            if not 'input' in ex:
                print(f"Missing initial input for example: {ex}")
                sys.exit(1)

            cmd = ['tezos-client',
                   'run', 'script', str(ex['path']),
                   'on', 'storage', str(ex['initial_storage']),
                   'and', 'input', str(ex['input'])]
            out = subprocess.Popen(
                cmd,
                stdout = subprocess.PIPE,
                stderr = subprocess.STDOUT)

            stdout,stderr = out.communicate()
            stdout = stdout.decode("utf-8").split("\n")

            error = None
            try:
                # print(stdout)
                idx1 = list(map(lambda s: s.strip(), stdout)).index("storage")
                idx2 = list(map(lambda s: s.strip(), stdout)).index("emitted operations")
                expected = str(ex['final_storage'])
                # print(stdout[idx])
                # print(stdout[idx+1])
                obtained = ("".join(stdout[(idx1+1):idx2])).strip()
                if expected != obtained:
                    error = f"Final storage is `{obtained}`, expected `{expected}` "
            except ValueError:
                error = f'Could not find final storage, script error?'

            if error != None:
                print(" ✗ " + ex['path'])
                print("  => " + error)
                print("  => command: " + (" ".join(cmd)))
            else:
                print(" ✔ " + ex['path'])



if __name__ == '__main__':
    main()
