{% import 'macros.html' as macros %}

{{ macros.linkable_header(2, 'Introduction', 'introduction', 'section') }}

This is a reference to the Michelson language used for smart contract
programming for the Tezos blockchain. It contains a reference to the
types and instructions of the language.

The examples used throughout the reference can be executed in the
browser using the <a href="{{try_michelson_url}}">Try Michelson</a>
interpreter. Just click "try it!" in the upper-right corner of the
example to load it in the interpeter:

{{ macros.show_example(try_michelson_url, "parameter unit;\nstorage unit;\ncode {CDR; NIL operation; PAIR};", "-", "Unit", "Unit") }}

In addition, the stack type of each location of the examples can be displayed
by hovering the mouse over that location.

{{ macros.linkable_header(2, 'Execution environment', 'execution', 'section') }}


A well-typed Michelson program implements a function which transforms a pair of Michelson
values representing the transaction parameter and the contract storage, and
returns a pair containing a list of internal operations and the new storage.
Some Michelson instructions make implicit use of data from an execution
environment, derived from the current state of the blockchain (the context),
from the transaction that triggered the contract, and from the block
containing this transaction. These instructions are categorized under
<a href="#instructions-blockchain">blockchain operations</a>.

{{ macros.linkable_header(2, 'Terminology', 'terminology', 'section') }}

<dl>
<dt>Instruction</dt>
<dd>Instructions refer to Michelson primitives such as <code>ADD</code>.
    Not to be confused with operations. </dd>
<dt>Operation</dt>
<dd>The final stack after a contract execution is a pair containing a new storage and a list of operations.
    An operation is either a transfer, an account creation or a delegation.</dd>
<dt>Numerical type</dt>
<dd>Refers to any of the numerical types of Michelson:
    <code>int</code>,
    <code>nat</code>,
    <code>timestamp</code> or
    <code>mutez</code>.
    Numerical values are values of one these types.
<dt>Sequence type</dt>
<dd>Refers to any of the two sequence types of Michelson: <code>string</code> or <code>bytes</code>.
</dd>
<dt>Structural type</dt>
<dd>Refers to any of the structural types of Michelson:
    <code>list</code>,
    <code>set</code>,
    <code>map</code> or
    <code>big_map</code>.
    Structural values are values of one these types.
</dd>
<dt>Argument</dt>
<dd>Michelson instructions may be indexed by arguments.
    For instance, the
    arguments of <code>PUSH nat 3</code> are <code>nat</code> and <code>3</code>.
    The argument of <CODE>DIP 2</CODE> is <code>2</code>.
    Not to be confused with operands and parameters.
</dd>
<dt>Operand</dt>
<dd>Refers to stack values consumed by instructions.
    Not to be confused with arguments and parameters.
</dd>
<dt>Parameter</dt>
<dd>Parameter may refer to three things.
    First, each contract has a parameter type and when called, takes a parameter value of this type.
    Second, some Michelson data types, such as <code>list</code> are parametric.
    For instance, a list of integer <code>list nat</code> is an instance of the list type with
    the type parameter instantiated to <code>nat</code>.
    Finally, the input type of a <code>lambda</code>.
    Not to be confused with arguments and operands.
</dd>
<dt>Return value</dt>
<dd>Refers either to the stack values produced by an instruction, or the return value of a <code>lambda</code>.
</dd>

<dt>Transaction</dt>

<dd>The execution of a contract is triggered by a blockchain operation
called a transaction, which is part of a block. A transaction has parameters
which are made available to the contracts in two different ways. The
argument of the transaction (a Michelson value) is passed to the contract as
the right side of a pair value, the only element in the initial stack.
Other parameters of the transaction (such as its amount, the implicit account
or contract that triggered it), and data from the block containing the
transaction (its timestamp) can be accessed by specific instructions. </dd>

<dt>Context</dt>

<dd>The context is a value representing the state of the blockchain after
the application of an operation. A contract execution may access data from the
context over which the execution is triggered. It has access to the
previous storage of the contract as the left side of a pair value, the only
element in the initial stack. Besides, some instructions use or extract
information from the context (current contract balance, contracts type).
</dd>

<dt>Storage</dt>
<dd> The storage of a contract is a Michelson value that represents it state.
Initial storage is provided at contract deployement (a.k.a. origination).
Subsequently, transactions trigger contract execution, in which a new store
is computed, based notably on the previous storage and the transaction argument.
</dd>

</dl>

{{ macros.linkable_header(2, 'Types', 'types', 'section') }}

See below for an explanation of type attributes.

<div class="table-container">
    {{ macros.quick_ref_tbl_ty(lang_def.get_types(), lang_def.get_type_attributes()) }}
</div>

Michelson data types (and by extension, the values of each type) can
be characterized by the following type attributes:

<dl>
<dt>Comparable</dt>
<dd>Comparable values can be stored in sets, can be passed as argument to <code>COMPARE</code>, etc.</dd>
<dt>Passable</dt>
<dd>Passable types are those that can be taken as a parameter in contracts.</dd>
<dt>Storable</dt>
<dd>Storable types can be used as a storage in contracts.</dd>
<dt>Pushable</dt>
<dd>Literal values of pushable types can be given as parameter to the <code>PUSH</code> primitive.</dd>
<dt>Packable</dt>
<dd>Values of packable types can be given as serialized using the <code>PACK</code> primitive.</dd>
<dt><code>big_map</code> value</dt>
<dd>These are types that be used in the domain of <code>big_map</code>s.</dd>
</dl>

The attributes of each type is given in the table above. All pushable
types are also packable and vice versa.  Except for the domain
specific types <code>operation</code>, <code>contract</code> and <code>big_map</code>, all types are
passable, storable, pushable, packable and can be used as values in
<code>big_map</code>s.


<!-- <script src="js/scripts.js"></script> -->
<!-- <h2>Overview</h2> -->
{{ macros.linkable_header(2, 'Instructions', 'instructions', 'section') }}
<div class="table-container">{{ macros.quick_ref_tbl(lang_def.get_instructions()) }}</div>

{{ macros.linkable_header(2, 'Instructions by Category', 'instructions-by-category', 'section') }}
{% for cat, title in lang_def.get_categories().items() %}
{{ macros.linkable_header(3, title, 'instructions-' ~ cat, 'section') }}
<div class="table-container">
    {{ macros.quick_ref_tbl(lang_def.get_instructions_by_category(cat)) }}
</div>
{% endfor %}

{{ macros.linkable_header(2, 'Type Reference', 'type-reference', 'section') }}

{% for type in lang_def.get_types() %}
{% include 'type.html' %}
{% endfor %}

{{ macros.linkable_header(2, 'Instruction Reference', 'instruction-reference', 'section') }}

{% for instr in lang_def.get_instructions() %}
{% include 'instruction.html' %}
{% endfor %}

