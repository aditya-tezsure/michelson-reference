import yaml
import json
import sys
import re
import os.path
from jsonschema import validate

def synthesize_stack_effect(instr):
    def ty_rule_to_stack_effect(r):
        se = list(map(lambda s: s.strip(),
                      re.split(r'[-=]>', r['conclusion'].split("::")[1])))
        assert len(se) == 2
        return se
    return list(map(ty_rule_to_stack_effect, instr['ty']))


class LanguageDefinition():

    default_instruction_meta = {'category': 'core', 'documentation_short': '--'}
    default_type_meta = {'documentation_short': '--',
                         'comparable': False,
                         'pushable': False,
                         'passable': False,
                         'storable': False,
                         'packable': False,
                         'unpackable': False,
                         'examples': [],
                         'insertions': []
    }

    def __init__(self,
                 language_meta_file = 'michelson-meta.yaml',
                 language_meta_schema_file = 'michelson-meta-schema.json',
                 language_definition_file = 'michelson.json',
                 # if false, then examples are not loaded
                 example_path = '../../../src/bin_client/test/contracts/',
                 strict=False):
        self.strict = strict

        self.language_meta_file = language_meta_file
        self.language_meta_schema_file = language_meta_schema_file
        self.language_definition_file = language_definition_file
        self.example_path = example_path

        self.instructions = False
        self.instructions_by_category = False
        self.types = False
        self.categories = False

        self.load_language_def()

    def opt_error(self, msg):
        msg = ("Error: " if self.strict else "Warning: ") + msg
        print(msg, file=sys.stderr)
        if self.strict:
            sys.exit(1)

    def is_instruction(self, op_name):
        return any(i['op'] == op_name for i in self.get_instructions())

    def is_type(self, ty_name):
        return any(t['ty'] == ty_name for t in self.get_types())

    def get_instructions(self):
        if not self.instructions:
            self.load_language_def()
        return self.instructions

    def get_instructions_by_category(self, cat):
        if not self.instructions_by_category:
            self.load_language_def()
        return self.instructions_by_category[cat]

    def get_types(self):
        if not self.types:
            self.load_language_def()
        return self.types

    def get_type_attributes(self):
        return [
            ('comparable', 'Comparable', 'C', 'Can be compared'),
            ('passable', 'Passable', 'PM', 'Can be taken as parameter'),
            ('storable', 'Storable', 'S', 'Can be put in storage'),
            ('pushable', 'Pushable', 'PU', 'Can be pushed'),
            ('packable', 'Packable', 'PA', 'Can be packed'),
            ('big_map_value', 'big_map value', 'B', 'Can be stored in big_maps')
        ]

    def get_categories(self):
        if not self.categories:
            self.load_language_def()
        return self.categories

    def merge_language(self, lang_def, lang_meta):
        # merge instructions
        self.instructions = []
        for op, instr in lang_def['instructions'].items():
            # Load meta information
            if not op in lang_meta['instructions']:
                self.opt_error("The instruction {} is undocumented".format(op))
                meta = LanguageDefinition.default_instruction_meta
            else:
                meta = lang_meta['instructions'][op]
            instr = {**instr, **meta}

            # Check rules
            if not len(instr['ty']):
                self.opt_error(f"The instruction {op} has no typing rules")
            if not len(instr['semantics']):
                self.opt_error(f"The instruction {op} has no semantics rules")

            assert ('category' in instr),\
                f"Ill-formed michelson-meta.yaml: instruction {op} has no category"

            # Verify category
            assert (instr['category'] in lang_meta['categories']),\
                f"Ill-formed michelson-meta.yaml: instruction {op} has non-existant category {instr['category']}"

            self.instructions.append(instr)


        self.instructions.sort(key=lambda i: i['op_args'])

        # merge types
        self.types = []
        print("load types", file=sys.stderr)
        for ty, ty_descr in lang_def['types'].items():
            if not ty in lang_meta['types']:
                self.opt_error("The type {} is undocumented".format(ty))
                meta = {}
            else:
                meta = lang_meta['types'][ty]
                meta = { **LanguageDefinition.default_type_meta,  **meta }

            self.types.append({**ty_descr, **meta})
        self.types.sort(key=lambda i: i['ty'])

    def load_instructions(self, lang_meta):
        for instr in self.instructions:
            op = instr['op']

            # Synthesize stack effet from typing rule
            if not 'stack_effect' in instr and len(instr['ty']):
                instr['stack_effect'] = synthesize_stack_effect(instr)
            if not 'stack_effect' in instr or not len(instr['ty']):
                print("Warning: The instruction {} has no stack effect, nor could it be inferred from typing rules"
                      .format(op), file=sys.stderr)
                instr['stack_effect'] = False

            # Load examples
            if 'examples' in instr:
                for idxe, ex in enumerate(instr['examples']):
                    # Load from file if path is given
                    assert 'path' in ex, f'missing path in example {ex}'

                    instr['examples'][idxe]['slug'] = ex['path']
                    instr['examples'][idxe]['path'] = os.path.join(self.example_path, ex['path'])

                    if os.path.exists(instr['examples'][idxe]['path']):
                        f = open(instr['examples'][idxe]['path'])
                        instr['examples'][idxe]['code'] = f.read()
                        f.close()
                    else:
                        print(f"Error: Could not find the file {instr['examples'][idxe]['path']}")
                        sys.exit(1)

                    if not 'hide_final_storage' in ex:
                        instr['examples'][idxe]['hide_final_storage'] = False
            else:
                instr['examples'] = []

            # Fill out optional properties
            if not 'documentation' in instr:
                instr['documentation'] = False

        self.instructions_by_category = {}

        self.categories = lang_meta['categories']
        for cat in lang_meta['categories']:
            self.instructions_by_category[cat] = [ i for i in self.instructions if i['category'] == cat ]


    def load_types(self):
        print("load types", file=sys.stderr)
        for ty_descr in self.types:
            ty = ty_descr['ty']

            # Load examples
            if 'examples' in ty_descr:
                for idxe, ex in enumerate(ty_descr['examples']):
                    # if type is not given explicitely for the
                    # example, then the type is not parametric.
                    if type(ex) is not list:
                        ty_descr['examples'][idxe] = (ex, ty_descr['ty'])

            # if polymorphic type, add some instantiations from
            # examples. these are used in tests
            if ty_descr['ty'] != ty_descr['ty_args'] and 'examples' in ty_descr:
                insertions = [ ty for (val, ty) in ty_descr['examples'] ]
            else:
                insertions = [ ty_descr['ty'] ]
            ty_descr['insertions'] = ty_descr['insertions'] + insertions

    def load_language_def(self):
        """Loads the michelson language definition.

        Uses michelson.json and michelson-meta.yaml. If strict is True,
        fails if any instructions are undocumented.
        """

        # load and validate meta data
        lang_meta = yaml.safe_load(open(self.language_meta_file, 'r'))
        lang_meta_schema = json.load(open(self.language_meta_schema_file, 'r'))
        validate(lang_meta, schema=lang_meta_schema)

        # load language semantics
        lang_def = json.load(open(self.language_definition_file))

        # collate meta and language definition for instruction
        self.merge_language(lang_def, lang_meta)

        if self.example_path:
            # load examples and fill out some additional information
            self.load_instructions(lang_meta)
            self.load_types()

    def write_merged(self, file):
        json.dump({
            'instructions': self.instructions,
            'types': self.types
        }, file, indent = 2)
