#!/bin/bash

TEZOS_ROOT=../../..
CONTRACTS_DIR=${TEZOS_ROOT}/src/bin_client/test/contracts
TEST_CONTRACTS_DIR=test/test-contracts

for i in `ls $CONTRACTS_DIR/*/*.tz $TEST_CONTRACTS_DIR/*/*.tz`; do
    if x=$( pygmentize -l MichelsonLexer.py:MichelsonLexer -f raw -x $i | grep Error ); then
		echo "Unlexed parser token in error in $i: $x"
		exit 1
    else
        echo "$i OK"
    fi
done


echo "All contracts successfully lexed."
