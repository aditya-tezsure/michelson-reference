#!/bin/bash

if [[ -z $1 ]]; then
	echo 'Usage: python {__file__} <contract.tz>'
	exit 1
fi


pygmentize -v -l MichelsonLexer.py:MichelsonLexer -x $1
