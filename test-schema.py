import yaml
import json
import sys
import re
import os.path
from jsonschema import validate

language_meta_file = 'michelson-meta.yaml'
language_meta_schema_file = 'michelson-meta-schema.json'

lang_meta = yaml.safe_load(open(language_meta_file, 'r'))
lang_meta_schema = json.load(open(language_meta_schema_file, 'r'))

validate(lang_meta, schema=lang_meta_schema)

print("michelson-meta.yaml is valid")
