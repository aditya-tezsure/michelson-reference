python=python3
tezos_client=tezos-client
ott?=ott
TEZOS_HOME?=~/dev/tezos/

all: site

%.json: %.ott
	$(ott) -i $< -o $@

michelson_embed.tex: michelson.ott
	$(ott) -tex_wrap false -coq_expand_list_types false -i $< -o $@

michelson.tex: michelson.ott
	$(ott) -coq_expand_list_types false -i $< -o $@

michelson.html: michelson.tex
	hevea michelson.tex

DOC_DEPS=generate.py michelson.json michelson-meta.yaml generate.py $(shell find templates -iname \*.html)

templates/rules: michelson_embed.tex
	mkdir -p $@
	$(python) pp_latex_rules.py

michelson.merged.json:
	$(python) generate_meta.py $@

docs/index.html: ${DOC_DEPS} $(shell find static -iname \*.js -or -iname \*.css) michelson_embed.tex templates/rules michelson.merged.json
	mkdir -p docs/static/
	cp -rv static/* docs/static/
	cp michelson.merged.json docs/michelson.json
	$(python) generate.py --example-path $(TEZOS_HOME)/tests_python/contracts/ --output $@

site: docs/index.html

docs/michelson_reference.html: ${DOC_DEPS}
	$(python) generate.py --standalone > $@

examples_typecheck:
	for i in `ls example-contracts/*.tz`; do tezos-client typecheck script $$i; done

examples_verify:
	$(python) examples-verify.py

test_lexer:
	./test-lexer.sh

test: examples_verify test_lexer

clean:
	rm -rf docs/* michelson.tex templates/rules michelson.html

touch-ott:
	touch michelson.ott

check-commited: touch-ott michelson.json michelson_embed.tex
	@if git diff --exit-code ; then\
		echo "\n[OK] michelson.json and michelson_embed.tex are up-to-date.\n";\
	else \
		echo "\n[ERROR] Some commited generated file is not up-to-date with michelson.ott\n";exit 1;\
	fi

test-docker:
	docker build . -t test_build
